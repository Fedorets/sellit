import { Component, Input } from '@angular/core';

@Component({
    selector: 'sell-product-item',
    templateUrl: './product-item.component.html',
    styleUrls: ['./product-item.component.scss']
})

export class ProductItemComponent {
    @Input() public products = [];
}
