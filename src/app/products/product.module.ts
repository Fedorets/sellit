import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from '../shared/shared.module';

import { routesProduct } from './product.routes';
import { ProductListComponent } from './product-list/product-list.component';
import { ProductItemComponent } from './product-item/product-item.component';
import { ProductsDataService } from './shared/products-data.service';

@NgModule({
    declarations: [
        ProductListComponent,
        ProductItemComponent
    ],
    imports: [
        BrowserModule,
        SharedModule,
        RouterModule.forChild(routesProduct)
    ],
    exports: [
        ProductItemComponent,
        ProductListComponent
    ],
    providers: [
        ProductsDataService
    ]
})

export class ProductModule { }
