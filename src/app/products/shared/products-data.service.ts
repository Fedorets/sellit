import { Injectable } from '@angular/core';
import { Http, Response, URLSearchParams } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';

import { ConfigService } from '../../core/services/config.service';
import { Product } from '../../shared/models/product.model';

@Injectable()
export class ProductsDataService {
    public offsetOfQuery: number;
    public limitOfQuery: number = 10;
    public searchOfQuery: any;
    constructor(private http: Http,
                private configService: ConfigService) {}

    /**
     * @param offsetQuery - offset parametr for URLSearchParams
     * @param searchOfQuery - search parametr for URLSearchParams
     * @returns array with Products objects
     */
    public getProducts(offsetQuery, ...searchOfQuery): Observable<Product[]> {
        this.searchOfQuery = searchOfQuery;
        this.offsetOfQuery = offsetQuery;
        return this.http.get(this.configService.activeSrc + 'poster',
            {search: this.params()})
            .map((res: Response) => {
                let data = res.json();
                let tmp = [];
                data.results.forEach((item) => {
                    tmp.push(new Product(item));
                });
                return tmp;
            });
    }

    /**
     * @returns object with http parameters
     */
    public params() {
        let params: URLSearchParams = new URLSearchParams();
        params.set('limit', this.limitOfQuery.toString() );
        params.set('offset', this.offsetOfQuery.toString() );
        if (this.searchOfQuery.length > 0) {
            params.set('search', this.searchOfQuery);
        }
        return params;
    }
}
