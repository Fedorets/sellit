import { Routes } from '@angular/router';

import { DetailComponent } from '../detail-page/detail-page.component';

export const routesProduct: Routes = [
    { path: 'product/:id', component: DetailComponent },
];
