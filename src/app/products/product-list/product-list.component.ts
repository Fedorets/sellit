import { Component, OnDestroy, OnInit } from '@angular/core';

import { ProductsDataService } from '../shared/products-data.service';
import { Subscription } from 'rxjs/Subscription';
import { SearchService } from '../../shared/components/header/search/search.service';

@Component({
    selector: 'sell-product-list',
    templateUrl: './product-list.component.html',
    styleUrls: ['./product-list.component.scss']
})

export class ProductListComponent implements OnInit, OnDestroy {
    public subscription: Subscription;
    public products: any = [];
    public offsetOfQuery: number = 0;
    public searchOfQuery: string = '';
    public searchOffset: number = 0;
    public flag: boolean = true;
    public noProducts: boolean = false;
    constructor ( private productDataService: ProductsDataService,
                  private searchService: SearchService ) {
        this.subscription = this.searchService.isListenSearch().subscribe((res) => {
            this.searchOfQuery = res;
            this.products = [];
            this.searchOffset = 0;
            this.addProducts();
        });
    }

    /**
     * Call in infinite scroll
     */
    public addProducts(): any {
        if (this.searchOfQuery.length === 0) {
            this.addUsualProducts();
        } else {
            this.addSearchProducts(false);
        }
    }
    public addUsualProducts(): any {
        if (this.flag) {
            this.offsetOfQuery += this.productDataService.limitOfQuery;
            this.productDataService.getProducts(this.offsetOfQuery).subscribe((res) => {
                res.forEach((item) => {
                    this.products.push(item);
                });
                this.flag = this.products.length !== length;
            }, (error) => {
                console.log('Error ', error);
            });
        }
        console.log(this.products);
    }
    public addSearchProducts(...more): any {
        console.log('Here');
        this.searchOffset += this.productDataService.limitOfQuery;
        this.productDataService.getProducts(this.searchOffset, this.searchOfQuery).subscribe((res) => {
            res.forEach((item) => {
                this.products.push(item);
            });
            this.noProducts = this.products.length === 0;
        }, (error) => {
            console.log('Error ', error);
        });
        console.log(this.products);
    }
    public ngOnInit() {
        this.productDataService.getProducts(this.offsetOfQuery).subscribe((res) => {
            this.products = res;
        });
    }
    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
