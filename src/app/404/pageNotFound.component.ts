import { Component } from '@angular/core';

@Component({
    selector: 'sell-error',
    templateUrl: './pageNotFound.component.html',
    styleUrls: ['./pageNotFound.component.scss']
})

export class PageNotFoundComponent {}
