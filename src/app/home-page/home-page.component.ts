import {
  Component
} from '@angular/core';

@Component({
  selector: 'sell-home-page',
  styleUrls: [ './home-page.component.scss' ],
  templateUrl: './home-page.component.html'
})
export class HomeComponent {
}
