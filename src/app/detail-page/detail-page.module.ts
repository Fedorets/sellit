import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { SliderComponent } from './slider/slider.component';
import { ProductDataService } from './shared/product-data.service';
import { DetailComponent } from './detail-page.component';
import { SharedModule } from '../shared/shared.module';
import { ChatModule } from '../chat/chat.module';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { EditPostComponent } from './edit-post/edit.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';

@NgModule({
    declarations: [
        SliderComponent,
        DetailComponent,
        EditPostComponent
    ],
    imports: [
        SharedModule,
        CommonModule,
        ChatModule,
        NgbModule,
        FormsModule,
        ReactiveFormsModule,
        FroalaEditorModule,
        FroalaViewModule,
    ],
    exports: [
        SliderComponent,
        DetailComponent,
        EditPostComponent
    ],
    providers: [
        ProductDataService
    ],
    entryComponents: [
        EditPostComponent
    ]
})

export class DetailModule {
}
