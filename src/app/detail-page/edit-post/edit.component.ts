import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import swal from 'sweetalert2';

import { RegExpService } from '../../shared/services/regExp.service';
import { ProductDataService } from '../shared/product-data.service';
import { PhotosService } from '../../shared/services/photos.service';
import { CharacterWatcher } from '../../shared/validators/characterWatcher.validator';

@Component({
    selector: 'sell-edit',
    templateUrl: './edit.component.html',
    styleUrls: ['./edit.component.scss']
})

export class EditPostComponent implements OnInit {
    @Input() public product: any = {};
    public editPostForm: FormGroup;
    public maxLengthDescr: number = 550;
    public photos: any = [];
    public currentForm: any;
    public status;
    public currentPhotos: any = {
        files: [],
        reader: []
    };
    public productsPhotoId = [];
    public options: Object = {
        placeholderText: 'Edit description',
        heightMin: 150,
        editorClass: 'textareaEditor'
    };
    constructor(public activeModal: NgbActiveModal,
                private regExp: RegExpService,
                private productService: ProductDataService,
                private photosService: PhotosService) {
    }

    /**
     * After submit - create object with required properties and subscribe on result updateProfileInfo http request
     * @param $event - click on button submit form
     * @param form - object with inputs values
     */
    public submit($event, form) {
        $event.preventDefault();
        this.currentForm = form;
        this.status = 'Sending data, please wait...';
        if (this.currentPhotos.files.length !== 0) {
           this.addNewProductPhotos();
        } else { this.updateProductInfo(); }
    }

    public addNewProductPhotos() {
        return this.photosService.addNewPhotos(this.currentPhotos.files).subscribe((res) => {
            res.forEach((i) => {
                this.productsPhotoId.push(i.id);
            });
            this.updateProductInfo();
        });
    }

    public updateProductInfo() {
        this.product.photos.forEach((index) => {
            this.productsPhotoId.push(index.id);
        });
        let formObj = {
            title: this.currentForm.title,
            description: this.currentForm.description,
            price: this.currentForm.price,
            author: this.product.userDetails.id,
            date_create: new Date(),
            photos: this.productsPhotoId
        };
        return this.productService.updateProductInfo(this.product.id, formObj).subscribe(
            (res) => {
                this.activeModal.close(res);
                swal({
                    title: 'Success',
                    text: 'The product was changed!',
                    type: 'success',
                    confirmButtonText: 'Good job!',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                });
            },
            (error) => {
                swal({
                    title: 'Error',
                    text: 'The product was not changed. Sorry.',
                    type: 'error',
                    confirmButtonText: 'Okey, I will try one more time.',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                });
            });
    }

    /**
     * For take info from fileReader directive
     * @param photosInfo - change input[type='file']
     */
    public addMiniature(photosInfo) {
        this.currentPhotos.reader.push(photosInfo.reader);
        this.currentPhotos.files = photosInfo.files;
    }

    /**
     * Use for remove 'fresh' photo
     * @param index - number of element in array currentPhotos
     */
    public removeCurrentPhoto(index) {
        this.currentPhotos.reader.splice(index, 1);
    }

    /**
     *
     * @param id - id photo, which is deleted
     * @returns {Subscription}
     */
    public removePhoto(id) {
        return this.photosService.deleteImages(id).subscribe((res) => {
            for (let i in this.product.photos) {
                if (this.product.photos.hasOwnProperty(i)) {
                    if (this.product.photos[i].id === id) {
                        this.product.photos.splice(i, 1);
                    }
                }
            }
        }, (error) => console.log(error) );
    }
    public ngOnInit() {
        this.editPostForm = new FormGroup({
            title: new FormControl(this.product.title, [Validators.required]),
            description: new FormControl(this.product.description, [ CharacterWatcher.watch(this.maxLengthDescr) ]),
            price: new FormControl(this.product.price, [Validators.required,
                Validators.pattern(this.regExp.price)]),
            photos: new FormControl()
        });
    }
}
