import { Component, Input } from '@angular/core';
import { NgbCarouselConfig } from '@ng-bootstrap/ng-bootstrap';

@Component({
    selector: 'sell-slider',
    templateUrl: './slider.component.html',
    styleUrls: ['./slider.component.scss'],
    providers: [ NgbCarouselConfig ]
})

export class SliderComponent {
    @Input() public photos;
    constructor(config: NgbCarouselConfig) {
        config.interval = 25000;
        config.wrap = false;
        config.keyboard = false;
    }
}
