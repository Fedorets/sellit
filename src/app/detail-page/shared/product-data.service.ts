import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';

import { ConfigService } from '../../core/services/config.service';
import { Product } from '../../shared/models/product.model';

@Injectable()
export class ProductDataService {
    constructor(private http: Http,
                private configService: ConfigService) { }

    /**
     * Get information for content detail-page
     * @param id of the product you want to extract
     * @returns Product object
     */
    public getSingleProduct(id): Observable<Product> {
        return this.http.get(`${ this.configService.activeSrc }poster/${ id }`)
            .map((res: Response) => {
                let data = res.json();
                return new Product(data);
            });
    }

    /**
     * @param id of the product you want to delete
     * @returns success or failure
     */
    public deleteProductInfo(id) {
        return this.http.delete(`${ this.configService.activeSrc }poster/${ id }`)
            .map((res: Response) => {
                return res.json();
            });
    }

    /**
     * Used after submit Edit Form
     * @param id product
     * @param data - properties of Edit Form
     */
    public updateProductInfo(id, data) {
        return this.http.put(`${ this.configService.activeSrc }poster/${ id }/`, data)
            .map((res: Response) => {
                return new Product(res.json());
            });
    }
}
