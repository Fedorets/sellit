import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import 'rxjs/';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Subscription } from 'rxjs/Subscription';
import swal from 'sweetalert2';

import { ProductDataService } from './shared/product-data.service';
import { AuthService } from '../core/services/auth.service';
import { EditPostComponent } from './edit-post/edit.component';
import { PhotosService } from '../shared/services/photos.service';

@Component({
    selector: 'sell-detail-page',
    templateUrl: './detail-page.component.html',
    styleUrls: ['./detail-page.component.scss']
})

export class DetailComponent implements OnInit, OnDestroy {
    public product: any = {};
    public userDetails: any = {};
    public id: number = 0;
    public isRoot: boolean = false;
    public isLogin: boolean = false;
    private authSubscription: Subscription;
    constructor(public route: ActivatedRoute, private router: Router,
                private productDataService: ProductDataService,
                private authService: AuthService,
                private photosService: PhotosService,
                private modalService: NgbModal) {
        route.params.subscribe((res) => {
            this.id = res.id;
        });
        this.authSubscription = this.authService.isListenAuthorization().subscribe((res) => {
            this.isLogin = res;
        });
    }

    /**
     * Start after click on edit button. Used for open in modal window EditPostComponent
     */
    public open() {
        const modalRef = this.modalService.open(EditPostComponent);
        modalRef.componentInstance.product = this.product;
        modalRef.result.then((result) => {
            this.product = result;
        }, (reason) => {
            console.log(`Dismissed ${reason}`);
        });
    }
     public ngOnInit() {
        this.productDataService.getSingleProduct(this.id).subscribe((res) => {
             this.product = res;
             console.log(res);
             this.userDetails = res.userDetails;
             (this.isLogin) ? this.isRoot = this.userDetails.id === this.authService.getUserData().id : this.isRoot = false;
        });
     }

    /**
     * Start after click on delete button. Subscribe on deleteProductInfo with parametr - ID of current product
     */
    public deleteProduct() {
        this.productDataService.deleteProductInfo(this.id).subscribe(
            (res) => {
                swal({
                    title: 'Success',
                    text: 'The product was deleted!',
                    type: 'success',
                    confirmButtonText: 'Good job!',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                }).then(() => {
                    this.router.navigate(['']);
                }, (dismiss) => {
                    this.router.navigate(['']);
                });
            },
            (error) => {
                swal({
                    title: 'Error',
                    text: 'The product was not deleted. Sorry.',
                    type: 'error',
                    confirmButtonText: 'Okey, I will try one more time.',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                });
            });
     }
     public ngOnDestroy() {
        this.authSubscription.unsubscribe();
    }
}
