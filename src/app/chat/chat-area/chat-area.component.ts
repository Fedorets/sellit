import { Component  } from '@angular/core';

@Component({
    selector: 'sell-chat-area',
    templateUrl: './chat-area.component.html',
    styleUrls: ['./chat-area.component.scss']
})

export class ChatAreaComponent {}
