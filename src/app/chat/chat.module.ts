
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChatComponent } from './chat.component';
import { SingleUserComponent } from './single-user/single-user.component';
import { ChatAreaComponent } from './chat-area/chat-area.component';
import { UsersListComponent } from './users-list/users-list.component';
import { ChatInputComponent } from './chat-input/chat-input.component';
import { SingleMessageComponent } from './single-message/single-message.component';
import { MessagesListComponent } from './messages-list/messages-list.component';

@NgModule({
    imports: [
        CommonModule
    ],
    exports: [
        ChatAreaComponent,
        SingleUserComponent,
        UsersListComponent,
        ChatInputComponent,
        ChatComponent,
        MessagesListComponent,
        SingleMessageComponent
    ],
    declarations: [
        ChatAreaComponent,
        ChatInputComponent,
        SingleUserComponent,
        UsersListComponent,
        ChatComponent,
        MessagesListComponent,
        SingleMessageComponent
    ]

})

export class ChatModule {}
