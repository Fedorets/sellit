import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';

import { SharedModule } from '../shared/shared.module';
import { NewPostComponent } from './new-post.component';
import { routes } from './new-post.routing';
import { NewPostService } from './new-post.service';
import { FroalaEditorModule, FroalaViewModule } from 'angular2-froala-wysiwyg';

@NgModule({
    declarations: [
        NewPostComponent
    ],
    imports: [
        RouterModule.forChild(routes),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        FroalaEditorModule,
        FroalaViewModule,
    ],
    exports: [
        NewPostComponent

    ],
    providers: [
        NewPostService
    ]
})

export class NewPostModule {}
