import { Component, ElementRef } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

import { RegExpService } from '../shared/services/regExp.service';
import { AuthService } from '../core/services/auth.service';
import { NewPostService } from './new-post.service';
import { PhotosService } from '../shared/services/photos.service';
import { CharacterWatcher } from '../shared/validators/characterWatcher.validator';

@Component ({
    selector: 'sell-new-post',
    templateUrl: './new-post.component.html',
    styleUrls: ['./new-post.component.scss']
})

export class NewPostComponent {
    public maxLengthDescr: number = 550;
    public currentPhotos: any = {
        files: [],
        reader: []
    };
    public status;
    public options: Object = {
        placeholderText: 'Edit description',
        heightMin: 150,
        editorClass: 'textareaEditor'
    };
    public formNewPost: FormGroup = new FormGroup({
        title: new FormControl(null, [Validators.required]),
        description: new FormControl(null, [ CharacterWatcher.watch(this.maxLengthDescr) ]),
        price: new FormControl(null, [Validators.required,
            Validators.pattern(this.regExp.price)]),
        photos: new FormControl(null)
    });
    constructor(private element: ElementRef,
                private regExp: RegExpService,
                private authService: AuthService,
                private router: Router,
                private newPostService: NewPostService,
                private photosServise: PhotosService) {}

    /**
     * Create object, subscribe on saving photo service and after subscribe on addNewPost
     * @param $event  - click on submit button
     * @param form - fields of NewPost form
     */
    public submit($event, form) {
        $event.preventDefault();
        let finishForm = {
            title: form.title,
            description: form.description,
            price: form.price,
            author: this.authService.getUserData().id,
            date_create: new Date(),
            photos: []
        };
        this.status = 'Sending data, please wait...';
        this.photosServise.addNewPhotos(this.currentPhotos.files).subscribe(
            (res) => {
                console.log('Res', res);
                res.forEach((index) => {
                    finishForm.photos.push(index.id);
                });
                this.newPostService.addNewPost(finishForm).subscribe((result) => {
                        swal({
                            title: 'Success',
                            text: 'The post was added!',
                            type: 'success',
                            confirmButtonText: 'Good job!',
                            confirmButtonClass: 'alert__confirm-btn',
                            buttonsStyling: false
                        }).then(() => {
                            this.router.navigate(['\product', result.id]);
                        }, (dismiss) => {
                            // dismiss can be 'overlay', 'cancel', 'close', 'esc', 'timer'
                            this.router.navigate(['\product', result.id]);
                        });
                    },
                    (error) => {
                    swal({
                        title: 'Error',
                        text: 'The post was not added. Sorry.',
                        type: 'error',
                        confirmButtonText: 'Okey, I will try one more time.',
                        confirmButtonClass: 'alert__confirm-btn',
                        buttonsStyling: false
                    });
                });
                },
            (error) => {
                swal({
                    title: 'Error',
                    text: 'The post was not added. Sorry.',
                    type: 'error',
                    confirmButtonText: 'Okey, I will try one more time.',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                });
            }
        );
    }

    /**
     * For watching changes and put them to the array this.photos
     * @param photosInfo - obj from fileReader directive
     */
    public addMiniature(photosInfo) {
        this.currentPhotos.reader.push(photosInfo.reader);
        this.currentPhotos.files = photosInfo.files;
    }

    /**
     * Use for remove photo from miniature
     * @param index - number of element in array currentPhotos
     */
    public removeCurrentPhoto(index) {
        this.currentPhotos.reader.splice(index, 1);
    }

    /**
     * For reset forms state after submit or click on reset button
     */
    public reset() {
        this.formNewPost.reset();
        this.currentPhotos.reader = [];
    }
}
