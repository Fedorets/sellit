import { Routes } from '@angular/router';

import { NewPostComponent } from './new-post.component';

export const routes: Routes = [
    { path: '', component: NewPostComponent, pathMatch: 'full' }
];
