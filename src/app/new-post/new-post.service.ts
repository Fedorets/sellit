import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigService } from '../core/services/config.service';

@Injectable()
export class NewPostService {
    constructor(private http: Http,
                private config: ConfigService) {}

    /**
     * @param form - fields from NewPost form
     * @returns object Product
     */
    public addNewPost(form) {
        return this.http.post(this.config.activeSrc + 'poster/', form)
            .map((res: Response) => {
            return res.json();
        });
    }
}
