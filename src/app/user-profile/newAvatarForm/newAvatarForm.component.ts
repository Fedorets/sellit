import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

import { ProfileService } from '../../core/services/profile.service';

@Component({
    selector: 'sell-new-avatar-form',
    templateUrl: './newAvatarForm.component.html',
    styleUrls: ['newAvatarForm.component.scss']
})
export class NewAvatarFormComponent {
    public profilePhoto = {
        reader: [],
        files: []
    };
    public status: string;
    public filesForm: any;
    public constructor( private activeModal: NgbActiveModal,
                        private profileService: ProfileService) {}
    /**
     * For watching changes and put them to the this.profilePhotos
     * @param currentPhoto - change input[type='file']
     */
    public addMiniature(currentPhoto) {
        this.profilePhoto = currentPhoto;
    }

    /**
     * After click on button change, update profile photo
     */
    public changePhoto() {
        if (this.profilePhoto.files) {
            this.filesForm = new FormData();
            this.filesForm.append('photo', this.profilePhoto.files[0]);
            this.status = 'Sending data, please wait...';
            this.profileService.uploadPhoto(this.filesForm)
                .then((res) => {
                    this.activeModal.close(res);
                    swal({
                        title: 'Success',
                        text: 'Photo was changed!',
                        type: 'success',
                        confirmButtonText: 'Good job!',
                        confirmButtonClass: 'alert__confirm-btn',
                        buttonsStyling: false
                    });
                }, (error) => this.status = 'Try again!');
        }
    }
    public closeActiveModal(state) {
        this.activeModal.dismiss(state);
    }
}
