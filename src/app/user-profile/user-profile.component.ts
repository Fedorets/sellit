import { Component, OnInit } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import swal from 'sweetalert2';

import { ProfileService } from '../core/services/profile.service';
import { ChangePasswordFormComponent } from './changePasswordForm/changePasswordForm.component';
import { NewAvatarFormComponent } from './newAvatarForm/newAvatarForm.component';

@Component ({
    selector: 'sell-user-profile',
    templateUrl: './user-profile.component.html',
    styleUrls: ['./user-profile.component.scss']
})

export class UserProfileComponent implements OnInit {
    public isDeleteAvatar: boolean = false;
    public user: any;
    public status: string;

    constructor( private profileService: ProfileService,
                 private modalService: NgbModal) {}

    /**
     * delete photo of current user after click on delete button
     */
    public deletePhoto() {
        this.status = 'Sending data, please wait...';
        this.profileService.deletePhoto(this.user.photo.id)
            .then((res) => {
                this.status = '';
                swal({
                    title: 'Success',
                    text: 'Photo was successfully deleted!',
                    type: 'success',
                    confirmButtonText: 'Good job!',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                });
                this.user = res;
                this.isDeleteAvatar = !!(this.user.photo.photo);
        }, (error) => this.status = 'Please, try again' );
    }

    /**
     * click on open modal windows: NewPhotoPopup and ChagePasswordPopup
     */
    public openNewPhotoPopup() {
        const modalRef = this.modalService.open(NewAvatarFormComponent);
        modalRef.result.then((res) => {
            if (res !== null) {
                this.user = res;
                this.isDeleteAvatar = !!(this.user.photo.photo);
            }
        }, (reason) => {
            console.log(`Dismissed ${reason}`);
        });
    }
    public openChangePasswordPopup() {
        this.modalService.open(ChangePasswordFormComponent);
    }

    public ngOnInit() {
        this.profileService.getUser().then((res) => {
            this.user = res;
            this.isDeleteAvatar = !!(this.user.photo.photo);
        });
    }
}
