import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { routesUserProfile } from './user-profile.routing';
import { AuthService } from '../login-page/auth.service';
import { UserProfileComponent } from './user-profile.component';
import { SharedModule } from '../shared/shared.module';
import { ChangePasswordFormComponent } from './changePasswordForm/changePasswordForm.component';
import { NewAvatarFormComponent } from './newAvatarForm/newAvatarForm.component';

@NgModule({
    declarations: [
        UserProfileComponent,
        ChangePasswordFormComponent,
        NewAvatarFormComponent
    ],
    imports: [
        RouterModule.forChild(routesUserProfile),
        SharedModule,
        FormsModule,
        ReactiveFormsModule,
        CommonModule,
        NgbModule
    ],
    exports: [
        UserProfileComponent
    ],
    entryComponents: [
        ChangePasswordFormComponent,
        NewAvatarFormComponent
    ]
})

export class UserProfileModule {
}
