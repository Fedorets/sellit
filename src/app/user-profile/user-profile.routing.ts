import { Routes } from '@angular/router';
import { UserProfileComponent } from './user-profile.component';

export const routesUserProfile: Routes = [
    { path: '', component: UserProfileComponent, pathMatch: 'full' }
];
