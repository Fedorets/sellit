import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ProfileService } from '../../core/services/profile.service';
import { RegExpService } from '../../shared/services/regExp.service';

@Component({
    selector: 'sell-change-password-form',
    templateUrl: './changePasswordForm.component.html',
    styleUrls: ['changePasswordForm.component.scss']
})

export class ChangePasswordFormComponent {
    public serverErrors: any = [];
    public status: string;
    public formChangePassword: FormGroup = new FormGroup({
        oldPassword: new FormControl(null, [Validators.required,
            Validators.minLength(6),
            Validators.pattern(this.RegExp.password)]),
        password: new FormControl(null, [Validators.required,
            Validators.minLength(6),
            Validators.pattern(this.RegExp.password)]),
        confirmPassword: new FormControl(null, [Validators.required,
            Validators.minLength(6)])
    });
    constructor( private profileService: ProfileService,
                 private router: Router,
                 private activeModal: NgbActiveModal,
                 private RegExp: RegExpService) {}

    /**
     * Send promise to profileService
     * @param $event - submit formChangePassword
     * @param value - fields of formChangePassword
     */
    public changePassword($event, value) {
        $event.preventDefault();
        swal({
            title: 'Info',
            text: 'You will be logged out after changing the password.',
            type: 'info',
            confirmButtonText: 'Okay, I am ready. Do it!',
            showCancelButton: true,
            cancelButtonText: 'Nooo',
            confirmButtonClass: 'alert__confirm-btn',
            cancelButtonClass: 'alert__cancel-btn',
            buttonsStyling: false
        }).then(() => {
            this.status = 'Sending data, please wait...';
            this.profileService.changePassword(value).then(
                (res) => {
                    this.activeModal.close(res);
                    swal({
                        title: 'Success',
                        text: 'Password was succesfully changed!',
                        type: 'success',
                        confirmButtonText: 'Good job!',
                        confirmButtonClass: 'alert__confirm-btn',
                        buttonsStyling: false
                    }).then(() => {
                        this.router.navigate(['']);
                    }, (dismiss) => {
                        this.router.navigate(['']);
                    });
                },
                (error) => {
                    this.status = '';
                    this.serverErrors = error;
                    console.log(error);
                }
            );
        }, (dismiss) => {
            console.log('No, meaning no');
        });
    }
    public closeActiveModal() {
        this.activeModal.dismiss('Cross click');
    }
}
