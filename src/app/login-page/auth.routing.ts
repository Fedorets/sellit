import { Routes } from '@angular/router';

import { LoginComponent } from './login-page.component';
import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';

export const routesAuth: Routes = [
    { path: '', component: LoginComponent, children: [
        { path: '', redirectTo: 'signIn', pathMatch: 'full' },
        { path: 'signUp', component: SignUpComponent },
        { path: 'signIn', component: SignInComponent }
    ]}
];
