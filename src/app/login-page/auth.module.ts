
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { SignUpComponent } from './sign-up/sign-up.component';
import { SignInComponent } from './sign-in/sign-in.component';
import { LoginComponent } from './login-page.component';
import { routesAuth } from './auth.routing';

import { SharedModule } from '../shared/shared.module';

@NgModule({
    declarations: [
        SignUpComponent,
        SignInComponent,
        LoginComponent
    ],
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        RouterModule.forChild(routesAuth),
        ReactiveFormsModule
    ],
    exports: [
        SignUpComponent,
        SignInComponent,
        LoginComponent
    ]
})

export class AuthModule {}
