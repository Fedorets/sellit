import { Component } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

import { RegExpService } from '../../shared/services/regExp.service';
import { AuthService } from '../../core/services/auth.service';
import { Registration } from '../../core/models/registration.model';

@Component({
    selector: 'sell-sign-up',
    templateUrl: './sign-up.component.html',
    styleUrls: ['./sign-up.component.scss']
})

export class SignUpComponent {
    public serverErrors: any = [];
    public formSignUp: FormGroup = new FormGroup({
        email: new FormControl(null, [Validators.required,
            Validators.pattern(this.regExp.email)]),
        username: new FormControl(null, [Validators.required]),
        password: new FormControl(null, [Validators.required,
            Validators.minLength(6),
            Validators.pattern(this.regExp.password)]),
        confirmPassword: new FormControl(null, [Validators.required])
    });
    constructor(private regExp: RegExpService,
                private authService: AuthService,
                private router: Router) {}

    /**
     * Call the method signUp in authService
     * @param $event - click on submit button
     * @param form - inputs values (email, username, password, confirmPassword)
     */
    public submit($event, form) {
        $event.preventDefault();
        this.authService.signUp(new Registration(form.value)).subscribe(
            (res) => {
                this.serverErrors = res;
                console.log('Result', res);
                if (res.success) {
                    swal({
                        title: 'Success',
                        text: 'Our congratulations! You are registered.',
                        type: 'success',
                        confirmButtonText: 'Hooray!!',
                        confirmButtonClass: 'alert__confirm-btn',
                        buttonsStyling: false
                    }).then(() => {
                        this.router.navigate(['login-page', 'signIn']);
                    }, (dismiss) => {
                        this.router.navigate(['login-page', 'signIn']);
                    });
                }
            },
            (error) => {
                console.log('Server Error!', error);
                this.serverErrors = error.non_field_errors;
            }
        );
    }
}
