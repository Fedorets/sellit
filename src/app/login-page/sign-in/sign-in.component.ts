import { Component } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';

import { validatePassword } from '../../shared/directives/validatorPassword.directive';
import { emailValidation } from '../../shared/directives/validatorEmail.directive';
import { RegExpService } from '../../shared/services/regExp.service';
import { AuthService } from '../../core/services/auth.service';
import { Login } from '../../core/models/login.model';

@Component({
    selector: 'sell-sign-in',
    templateUrl: './sign-in.component.html',
    styleUrls: ['./sign-in.component.scss']
})

export class SignInComponent {
    public serverErrors: any = [];
    public formSignIn: FormGroup = new FormGroup({
        email: new FormControl(null, [Validators.required,
            Validators.pattern(this.regExp.email)]),
        password: new FormControl(null, [Validators.required,
            Validators.minLength(6),
            Validators.pattern(this.regExp.password)])
    });
    constructor(private regExp: RegExpService,
                private authService: AuthService,
                private router: Router) {}

    /**
     * Call the method signIn in authService
     * @param $event - click on submit button
     * @param form - inputs values (email, password)
     */
    public submit($event, form) {
        $event.preventDefault();
        this.authService.signIn(new Login(form.value)).subscribe(
            (res) => {
                this.router.navigate(['']);
                this.serverErrors = [];
            },
            (error) => {
                swal({
                    title: 'Warning',
                    text: 'Please, check your personal information.',
                    type: 'warning',
                    confirmButtonText: 'Okey, I will try one more time.',
                    confirmButtonClass: 'alert__confirm-btn',
                    buttonsStyling: false
                });
                this.serverErrors = error.json();
            });
    }
}
