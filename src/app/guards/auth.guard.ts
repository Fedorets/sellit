import { Observable } from 'rxjs/Observable';
import { Injectable } from '@angular/core';
import {
    ActivatedRoute, ActivatedRouteSnapshot, CanActivate, Router,
    RouterStateSnapshot
} from '@angular/router';

import { AuthService } from '../core/services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
    constructor(private auth: AuthService,
                private router: Router) {
    }

    /**
     * Used for protection pages
     * @param next - current route
     * @returns permission
     */
    public canActivate(next: ActivatedRouteSnapshot,
                       state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
        let status: string = next.data.status;
        if (status === 'Admin' && this.auth.isLog()) {
            return true;
        } else if (status === 'Visitor' && !this.auth.isLog()) {
            return true;
        } else { this.router.navigate(['']); return false; }
    }
}
