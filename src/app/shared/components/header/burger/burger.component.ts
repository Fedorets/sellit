import { Component } from '@angular/core';

@Component({
    selector: 'sell-burger',
    templateUrl: './burger.component.html',
    styleUrls: ['./burger.component.scss']
})

export class BurgerComponent {
    public flag: boolean = false;

    /**
     * Active on click, open-close burger and mobile menu
     */
    public clickOnBurger () {
        this.flag = !this.flag;
        $('.user-block--authorized').toggleClass('user-block--authorized--open');
    };
}
