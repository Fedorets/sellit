import { Injectable } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/debounceTime';

@Injectable()
export class SearchService {
    public search: Subject<string> = new Subject<string>();
    public isListenSearch(): Observable<any> {
        return this.search.asObservable();
    }
    public changeValue(value) {
        this.search.next(value);
    }
}
