import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import { SearchService } from './search.service';
import { Router } from '@angular/router';

@Component({
    selector: 'sell-search',
    templateUrl: './search.component.html',
    styleUrls: ['./search.component.scss']
})

export class SearchComponent implements OnInit {
    public searchField: string;
    public searchChanged: Subject<string> = new Subject<string>();
    public constructor(private searchService: SearchService, private router: Router) {}

    /**
     * Active when value input changes and send it to service
     * @param value input Search
     */
    public changed(value: string) {
        this.searchService.changeValue(value);
    }

    /**
     * Active when user click on icon-search
     * @param value input Search
     */
    public searchGo(value) {
        this.router.navigate(['']);
    }
    public ngOnInit() {
        this.searchChanged
            .debounceTime(300)
            .distinctUntilChanged()
            .subscribe((search) => this.changed(search) );
    }
}
