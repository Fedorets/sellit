import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subscription } from 'rxjs/Subscription';

import { AuthService } from '../../../../core/services/auth.service';

@Component({
    selector: 'sell-user-panel',
    templateUrl: './user-panel.component.html',
    styleUrls: ['./user-panel.component.scss']
})

export class UserPanelComponent implements OnInit, OnDestroy {
    public isLogginIn: boolean;
    public user: any;
    public subscription: Subscription;
    constructor(private authService: AuthService,
                private router: Router) {
            this.subscription = this.authService.isListenAuthorization().subscribe((res) => {
                this.isLogginIn = res;
                this.user = this.authService.getUserData();
            });
    }

    /**
     * Active after click on logout button
     */
    public logout() {
        this.authService.logout().then(() => {
            this.router.navigate(['/']);
        });
    }
    public ngOnInit() {
        this.user = this.authService.getUserData();
    }
    public ngOnDestroy() {
        this.subscription.unsubscribe();
    }
}
