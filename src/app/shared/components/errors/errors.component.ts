import { Component, Input } from '@angular/core';

@Component({
    selector: 'sell-errors',
    templateUrl: './errors.component.html',
    styleUrls: ['./errors.component.scss']
})

export class ErrorsComponent {
    @Input() public form: any;
    @Input() public current: any;
    @Input() public serverErrors;
    public errors = {
        email: {
            required: 'Email is required',
            pattern: 'Email is not valid'
        },
        password: {
            required: 'Password is required',
            pattern: 'Password should consist of numbers and letters',
            minlength: 'Password should be more than 6',
        },
        confirmPassword: {
            required: 'Confirm password is required',
            validateEqual: 'Password mismatch'
        },
        username: {
            required: 'Username is required'
        },
        title: {
            required: 'Title is required'
        },
        description: {
            characterMax: 'Please, make your description smaller.'
        },
        price: {
            required: 'Price is required',
            pattern: 'Price should look like `10.02`'
        }
    };
}
