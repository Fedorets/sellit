import { AbstractControl } from '@angular/forms';

export class CharacterWatcher {
    public static watch(maxVal: number) {
        return (c: AbstractControl) => {
            if (c.value !== null) {
                let clearValue = c.value.replace(/(\<(\/?[^>]+)>)/g, '');
                // console.log(clearValue, clearValue.length);
                return clearValue.length > maxVal ?
                    { characterMax: true } :
                    null;
            }
        };
    }
}
