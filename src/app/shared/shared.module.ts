import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { HeaderComponent } from './components/header';
import { SearchComponent } from './components/header/search';
import { UserPanelComponent } from './components/header/user-panel';
import { BurgerComponent } from './components/header/burger';
import { FooterComponent } from './components/footer';
import { BtnScrollTopComponent } from './components/btnScrollTop';

import { ScrollTopDirective } from './directives/scrollTop.directive';
import { InfinitiScrollDirective } from './directives/infinitiScroll.directive';

import { RegExpService } from './services/regExp.service';
import { ErrorsComponent } from './components/errors/errors.component';
import { SearchService } from './components/header/search/search.service';
import { PhotosService } from './services/photos.service';
import { FileReaderDirective } from './directives/fileReader.directive';
import { MatchPasswordDirective } from './directives/matchPassword.directive';

@NgModule({
    declarations: [
        HeaderComponent,
        SearchComponent,
        UserPanelComponent,
        BurgerComponent,
        FooterComponent,
        BtnScrollTopComponent,
        ErrorsComponent,
        ScrollTopDirective,
        InfinitiScrollDirective,
        FileReaderDirective,
        MatchPasswordDirective
    ],
    imports: [
        CommonModule,
        RouterModule,
        FormsModule
    ],
    exports: [
        HeaderComponent,
        UserPanelComponent,
        BurgerComponent,
        FooterComponent,
        SearchComponent,
        ErrorsComponent,
        BtnScrollTopComponent,
        ScrollTopDirective,
        InfinitiScrollDirective,
        FileReaderDirective,
        MatchPasswordDirective
    ],
    providers: [
        RegExpService,
        SearchService,
        PhotosService
    ]
})

export class SharedModule {}
