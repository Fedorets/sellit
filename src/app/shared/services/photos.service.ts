import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/map';

import { ConfigService } from '../../core/services/config.service';

@Injectable()
export class PhotosService {
    public filesForm: any;
    constructor(private config: ConfigService, private http: Http) {}

    /**
     * Used in edit-post, detail-page components
     * @param data - array photos from input[type='file']
     * @returns array of Photos objects
     */
    public addNewPhotos(data) {
        if (data) {
            let files: FileList = data;
            this.filesForm = new FormData();
            for (let i in files) {
                if (files.hasOwnProperty(i)) {
                    this.filesForm.append('photo', files[i]);
                }
            }
        }
        console.log(this.filesForm);
        return this.http.post(this.config.activeSrc + 'photo/', this.filesForm)
            .map((res: Response) => {
                return res.json();
            });
    }

    /**
     * Used in edit-post, detail-page components
     * @param id of photo to delete
     * @returns success or failure
     */
    public deleteImages(id) {
        return this.http.delete(this.config.activeSrc + 'photo/' + id)
            .map((res: Response) => {
                return res.json();
            });
    }
}
