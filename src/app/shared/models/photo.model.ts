export class Photo {
    public id: number;
    public photo: string;
    constructor(data) {
       this.id = data.id;
       this.photo = data.photo;
    }
}
