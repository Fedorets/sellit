 import { Avatar } from './avatar.model';

export class User {
    public id: number;
    public firstName: string;
    public lastName: string;
    public email: string;
    public username: string;
    public photo: any;

    constructor(data) {
        this.id = data.id;
        this.firstName = data.first_name;
        this.lastName = data.last_name;
        this.email = data.email;
        this.username = data.username;
        this.photo = new Avatar(data.photo);
    }

    get fullName() {
        return this.firstName + ' ' + this.lastName;
    }

    public getAll() {
        return {
            id: this.id,
            first_name: this.firstName,
            last_name: this.lastName,
            email: this.email,
            username: this.username,
            photo: {
                id: this.photo.id,
                photo: this.photo._photo
            },
        };
    }
}
