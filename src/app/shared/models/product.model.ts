import { User } from './user.model';
import { Photo } from './photo.model';

export class Product  {

    private _id: number;
    private _title: string;
    private _description: string;
    private _price: number;
    private _photos = [];
    private _dateCreate: string;
    private _dateUpdate: string;
    private _userDetails: User;

    constructor(data) {
        this._id = data.id;
        this._title = data.title;
        this._description = data.description;
        this._price = data.price;
        this._dateCreate = data.date_create;
        this._dateUpdate = data.date_update;
        this._userDetails = new User(data.author_details);
        data.photo_details.forEach((index) => {
            this._photos.push(new Photo(index));
        });
    }
    get id() {
        return this._id;
    }

    get userDetails() {
        return this._userDetails;
    }

    get title(){
        return this._title;
    }

    set title(title) {
        this._title = title;
    }

    get description(){
        return this._description;
    }

    set description(desc) {
        this._description = desc;
    }

    get price() {
        return this._price;
    }

    set price(price) {
        this._price = price;
    }

    get photos() {
        return this._photos;
    }

    set photos(photos){
        this._photos.push(new Photo(photos));
    }

    get dateCreate() {
        return this._dateCreate;
    }

    set dateCreate(date) {
        this._dateCreate = date;
    }

    get dateUpdate() {
        return this._dateUpdate;
    }

    set dateUpdate(date) {
        this._dateUpdate = date;
    }
}
