import { Directive, ElementRef, EventEmitter, HostListener, Output } from '@angular/core';

@Directive({
    selector: '[sellit-file-reader]'
})

export class FileReaderDirective {
    @Output() public changeFiles: EventEmitter<any> = new EventEmitter<any>();
    constructor(private element: ElementRef) {}

    @HostListener('change', ['$event'])
    public change(event) {
        let filesList = this.element.nativeElement.files;
        // console.log(filesList);
        let objectInfo = {
            files: filesList,
            reader: []
        };
        for (let file of filesList) {
            let myReader: FileReader = new FileReader();
            myReader.onload = (e: any) => {
                objectInfo.reader = e.target.result;
                this.changeFiles.emit(objectInfo);
            };
            myReader.readAsDataURL(file);
        }
    }
}
