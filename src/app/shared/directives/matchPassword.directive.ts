
import { Directive } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, ValidationErrors } from '@angular/forms';

export function passwordChecking(control: AbstractControl): { [key: string]: any } {
    if (!control) {
        return null;
    } else if (control.get('password').value !== control.get('confirmPassword').value) {
        return { validateEqual: true };
    }
}

@Directive({
    selector: '[validateEqual]',
    providers: [{provide: NG_VALIDATORS, useExisting: MatchPasswordDirective, multi: true}]
})

export class MatchPasswordDirective {
    public validate(control: AbstractControl): ValidationErrors {
           return passwordChecking(control);
        }
}
