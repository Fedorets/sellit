import { Routes } from '@angular/router';
import { HomeComponent } from './home-page';
import { PageNotFoundComponent } from './404/pageNotFound.component';
import { AuthGuard } from './guards/auth.guard';

export const ROUTES: Routes = [
  { path: '',      component: HomeComponent, pathMatch: 'full' },
  { path: 'login-page', loadChildren: './login-page/auth.module#AuthModule',
    data: { status: 'Visitor' },
    canActivate: [AuthGuard]},
  { path: 'user-profile', loadChildren: './user-profile/user-profile.module#UserProfileModule',
    data: {status: 'Admin'},
    canActivate: [AuthGuard]},
  { path: 'new-post', loadChildren: './new-post/new-post.module#NewPostModule',
    data: { status: 'Admin' },
    canActivate: [AuthGuard]},
  { path: '**', component: PageNotFoundComponent }
];
