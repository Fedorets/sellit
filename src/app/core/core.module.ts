import { NgModule } from '@angular/core';
import { Http, RequestOptions, XHRBackend } from '@angular/http';

import { AuthService } from './services/auth.service';
import { ConfigService } from './services/config.service';
import { MyHttp } from './services/http.service';
import { ProfileService } from './services/profile.service';
import { SessionService } from './services/session.service';

@NgModule({
    imports: [],
    providers: [
        AuthService,
        ConfigService,
        {
            provide: Http,
            useClass: MyHttp,
            deps: [XHRBackend, RequestOptions, SessionService]
        },
        ProfileService,
        SessionService
    ]
})

export class CoreModule {
}
