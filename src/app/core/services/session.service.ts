import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie';

import { User } from '../../shared/models/user.model';

@Injectable()
export class SessionService {
    constructor(private cookie: CookieService) {
        // this.cookie.remove('token')
        // console.log(this.cookie.get('token'));
    }

    /**
     * Get token from cookie, used in guards, for checking if the user is logged in
     * @returns token
     */
    public get token() {
        return this.cookie.get('token');
    }

    /**
     * Put token to the cookie, used in signIn function
     * @param data - token
     */
    public set token(data) {
        (data) ? this.cookie.put('token', data, {expires: this.getDateExpire(10)}) : this.cookie.remove('token');
    }

    /**
     *
     * @param expireDays - number of days to store the token
     * @returns date of the token removal
     */
    public getDateExpire(expireDays) {
        let dateExpire = new Date();
        dateExpire.setTime(dateExpire.getTime() + expireDays * 24 * 60 * 60 * 1000);
        return dateExpire;
    }

    /**
     * Used for retrieve info about the user from localStorage
     * @returns object User or null
     */
    public get user() {
        let result;
        let user = JSON.parse(localStorage.getItem('user'));
        if (user !== null) {
            result = (this.token) ? new User(user) : null;
        }       else { result = null; }
        return result;
    }

    /**
     * Used to record info about user in the localStorage
     * @param data - object User
     */
    public set user(data) {
        localStorage.setItem('user', JSON.stringify(data.getAll()));
    }

    /**
     * Delete user from localStorage
     */
    public removeUser() {
        localStorage.removeItem('user');
    }
}
