import { Injector, Injectable, ReflectiveInjector } from '@angular/core';
import {
    async,
    getTestBed,
    TestBed
} from '@angular/core/testing';
import {
    BaseRequestOptions,
    Http,
    Response,
    ResponseOptions,
    XHRBackend
} from '@angular/http';
import {
    MockBackend,
    MockConnection
} from '@angular/http/testing';
import { ConfigService } from './config.service';
import { User } from '../../shared/models/user.model';
import { CookieOptions, CookieService, CookieModule } from 'ngx-cookie/index';
import { SessionService } from './session.service';
import { AuthService } from './auth.service';
describe('Service: Auth', () => {
    let backend: MockBackend;
    let service: AuthService;
    let cookieService: CookieService;
    let sessionService: SessionService;
    let testbed: Injector;
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                CookieModule.forRoot()
            ],
            providers: [
                BaseRequestOptions,
                MockBackend,
                ConfigService,
                SessionService,
                AuthService,
                {
                    deps: [
                        MockBackend,
                        BaseRequestOptions
                    ],
                    provide: Http,
                    useFactory: (backend: XHRBackend, defaultOptions: BaseRequestOptions) => {
                        return new Http(backend, defaultOptions);
                    }
                }
            ]
        });
        testbed = getTestBed();
        backend = testbed.get(MockBackend);
        service = testbed.get(AuthService);
        sessionService = testbed.get(SessionService);
        cookieService = testbed.get(CookieService);
    }));
    afterEach(() => {
        cookieService.removeAll();
        testbed = undefined;
        cookieService = undefined;
        sessionService = undefined;
    });
    function signInBackend(backend: MockBackend, options: any) {
        backend.connections.subscribe((connection: MockConnection) => {
            if (connection.request.url === 'http://fe-kurs.light-it.net:38000/api/login/') {
                const responseOptions = new ResponseOptions(options);
                const response = new Response(responseOptions);
                connection.mockRespond(response);
            }
        });
    }
    describe('Feature: isLog', () => {
        let isLog: boolean;
        describe('WHEN, token not exist', () => {
            beforeEach(() => {
                spyOnProperty(sessionService, 'token', 'get').and.returnValue(false);
                isLog = service.isLog();
            });
            it('THEN, method return false', () => {
                expect(isLog).toBeFalsy();
            });
        });
        describe('WHEN, token exist', () => {
            beforeEach(() => {
                spyOnProperty(sessionService, 'token', 'get').and.returnValue('token');
                isLog = service.isLog();
            });
            it('THEN, method return true', () => {
                expect(isLog).toBeTruthy();
            });
        });
    });
    // describe('Feature: signIn', () => {
    //     describe('WHEN, user is exist', () => {
    //         let callback: jasmine.Spy;
    //         beforeEach(() => {
    //             callback = jasmine.createSpy('callback');
    //             signInBackend(backend, {
    //                 body: [
    //                     {
    //                         id : 123,
    //                         first_name : '',
    //                         last_name : '',
    //                         email : 'test@email.co',
    //                         username : 'test',
    //                         token : 'some-token-value'
    //                     }
    //                 ],
    //                 status: 200
    //             });
    //             let data = {
    //                 email: 'test@email.co',
    //                 password: '12333'
    //             };
    //             service.signIn(data).subscribe((resp) => {
    //                 callback(resp);
    //             });
    //         });
    //         it('THEN, method return error', () => {
    //             expect(callback).toHaveBeenCalled();
    //         });
    //     });
    //     describe('WHEN, user is not exist', () => {
    //         beforeEach(() => {
    //         });
    //         it('THEN, method return user', () => {
    //         });
    //     });
    // });
    describe('Feature: signUp', () => {
       describe('When signUp is successful', () => {
           it('THEN, method returns successful message', (done) => {
               backend.connections.subscribe((connection: MockConnection) => {
                   let options = new ResponseOptions({
                              body: {
                                    success: 'User was successfully created.'
                               },
                           status: 200
                       });
                   connection.mockRespond(new Response(options));
                });
               service.signUp({email: 'test@gmail.com', username: 'gipo', password: 'qwerty1', password_confirm: 'qwerty1'})
                .subscribe((res) => {
                    expect(res).toBeDefined();
                    expect(res.success).toBeDefined();
                    done();
                   });
                });
            });
       });
});
