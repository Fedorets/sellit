import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { ConfigService } from './config.service';
import { User } from '../../shared/models/user.model';
import { SessionService } from './session.service';

@Injectable()
export class AuthService {
    private subject = new BehaviorSubject(this.isLog());
    constructor(private http: Http,
                private configService: ConfigService,
                private session: SessionService) {
        if (this.isLog()) {
            this.getProfile().subscribe();
        }
    }

    /**
     * @returns checking if session has token
     */
    public isLog() {
        return !!(this.session.token);
    }

    /**
     * Retrieve user information and put it to the localStorage
     * @returns object of User
     */
    public getProfile() {
        return this.http.get(`${ this.configService.activeSrc }profile/me/`)
            .map((res: Response) => {
                let user = new User(res.json());
                this.session.user = user;
                this.isSignIn();
                return user;
            });
    }

    /**
     * @param data - object consist of email, password, confirmPassword, username
     * @returns success or error
     */
    public signUp(data): any {
        return this.http.post(`${ this.configService.activeSrc }signup/`, data)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => { return Observable.throw(error); });
    }

    /**
     * For login authorization and saving token && user in the session
     * @param data - object consist of email and password
     * @returns object of User
     */
    public signIn(data): any {
        return this.http.post(`${ this.configService.activeSrc }login/`, data)
            .map((res: Response) => {
                let user = new User(res.json());
                this.session.token = res.json().token;
                this.session.user = user;
                this.isSignIn();
                return user;
            })
            .catch((error: any) => { return Observable.throw(error); });
    }

    /**
     * for subscription, is user autorized
     */
    public isListenAuthorization(): Observable<any> {
        return this.subject.asObservable();
    }

    /**
     * Used on header (user-panel)
     */
    public isSignIn() {
        if (this.session.token) {
            return this.subject.next(true);
        } else {
            this.session.removeUser();
            return this.subject.next(false);
        }
    }

    /**
     * @returns object User from localStorage
     */
    public getUserData() {
        return this.session.user;
    }

    /**
     * For remove data user from session (token and localStorage)
     */
    public logout() {
        return this.http.post(`${ this.configService.activeSrc }logout/`, '')
            .map((res: Response) => {
                this.session.token = null;
                this.session.removeUser();
                this.isSignIn();
                return res.json();
            })
        .toPromise();
    }

    /**
     * For remove data user from session (token and localStorage) after change password
     */
    public logoutAfterChangePassword() {
        this.session.token = null;
        this.session.removeUser();
        this.isSignIn();
        return true;
    }
}
