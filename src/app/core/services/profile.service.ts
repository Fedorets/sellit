import { Injectable } from '@angular/core';
import { ConfigService } from './config.service';
import { Http, Response } from '@angular/http';
import 'rxjs/add/operator/toPromise';
import { Observable } from 'rxjs/Observable';

import { AuthService } from './auth.service';
import { User } from '../../shared/models/user.model';
import { SessionService } from './session.service';

@Injectable()
export class ProfileService {
    constructor(private http: Http,
                private config: ConfigService,
                private authService: AuthService,
                private session: SessionService) {}

    /**
     * Retrieve user information and put it to the session
     * @returns object of User
     */
    public getUser() {
        return this.http.get(`${ this.config.activeSrc }profile/me/`)
            .map((res: Response) => {
                let user = new User(res.json());
                this.session.user = user;
                this.authService.isSignIn();
                return user;
            })
            .toPromise();
    }

    /**
     * @param data - formData variable with photo
     * @returns function-request for update profile photo with received ID
     */
    public uploadPhoto(data) {
        return this.http.post(`${ this.config.activeSrc }photo/`, data)
            .toPromise()
            .then ((res) => { return this.updateProfilePhoto(res.json()); });
    }

    /**
     * @param res - object with ID and src photo
     * @returns request for update all profile info
     */
    public updateProfilePhoto(res) {
        let result = res[0].id;
        let obj = {
            user: this.authService.getUserData().id,
            photo: result
        };
        return this.http.post(`${ this.config.activeSrc }profile_photo/`, obj)
            .toPromise()
            .then (() => { return this.getUser(); });
    }

    /**
     * @param id of photo
     * @returns request for update all profile info
     */
    public deletePhoto(id) {
        return this.http.delete(`${ this.config.activeSrc }photo/${ id }`)
            .toPromise()
            .then (() => { return this.getUser(); });
    }

    /**
     * @param data from form ChangePassword (pass, newPass, confirmPass)
     * @returns function for delete user from session
     */
    public changePassword(data) {
        let obj = {
            new_password1: data.password,
            new_password2: data.confirmPassword,
            old_password: data.oldPassword
        };
        return this.http.post(`${ this.config.activeSrc }change_password/`, obj)
            .map((res: Response) => {
                return res.json();
            })
            .catch((error: any) => { return Observable.throw(error.json()); })
            .toPromise()
            .then((res) => {
                this.authService.logoutAfterChangePassword();
            });
    }
}
